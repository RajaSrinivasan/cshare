package main

// Based on: https://developers.google.com/drive/api/v3/quickstart/go

import (
	"fmt"

	"gitlab.com/RajaSrinivasan/cshare/cmd"
)

func oldmain() {
	/*fmt.Println("Hello Go")
	xfer := gdrive.New()

	err := xfer.Initialize()
	if err != nil {
		fmt.Println("Failed to initialize")
		return
	}*/
	fmt.Println("Initialized client access.")
	//xfer.List("audio/mp3")
}

func main() {
	cmd.Execute()
}
