package gdrive

import (
	"fmt"
	"log"

	"google.golang.org/api/drive/v3"
)

func (GDriveShare) List(fsearch string) {

	fmt.Printf("Will search for %s\n", fsearch)
	srv, err := drive.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Drive client: %v", err)
	}
	lst := srv.Files.List()
	nextPageToken := ""
	numFiles := 0
	search := fmt.Sprintf("name contains '%s'", fsearch)
	for {
		r, err := lst.PageSize(100).Fields("nextPageToken, files(id, name, kind)").PageToken(nextPageToken).Q(search).Do()
		//r, err := lst.PageSize(100).Fields("nextPageToken, files(id, name, kind)").PageToken(nextPageToken).Q(mimetype).Do()
		if err != nil {
			log.Fatalf("Unable to retrieve files: %v", err)
		}
		if len(r.Files) == 0 {
			fmt.Println("No more files found.")
			break
		} else {
			for _, i := range r.Files {
				numFiles = numFiles + 1
				fmt.Printf("%04d : %s\n", numFiles, i.Name)
			}
			if len(r.NextPageToken) < 1 {
				fmt.Printf("End of files list")
				break
			}
			fmt.Printf("NextPage: %s\n", r.NextPageToken)
			nextPageToken = r.NextPageToken
		}
	}
}
