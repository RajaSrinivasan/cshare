package drivers

import (
	"fmt"

	"gitlab.com/RajaSrinivasan/cshare/impl/cloudshare"
)

func List(cl cloudshare.CloudShareType, arg string) {
	fmt.Printf("List argument %s\n", arg)
	err := cl.Initialize()
	if err != nil {
		fmt.Printf("Unable to initialize %s\n", err)
		return
	}
	cl.List(arg)
}
