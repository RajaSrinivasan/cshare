package cloudshare

type CloudShareType interface {
	Initialize() error
	List(names string)
}
